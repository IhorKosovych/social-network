# "Цитатник"



"Цитатник" is a web-application implemented with Django framework which allows you to create quote which you want.
# Functionality:

  - User Login;
  - User Sign Up; 
  - User Logout;
  - View all quotes;
  - Detailed view;
  - Add quotes.

To run application:
```sh
$ python manage.py runserver 
```
