from django.urls import path
 
from .views import BlogListView, upload, post_detail, like_view
 
urlpatterns = [
    path('', BlogListView.as_view(), name='home2'),
    path('upload/', upload, name = 'upload-post'),
    path("<int:pk>/", post_detail, name="post_detail"),
    path('<int:pk>/like/', like_view, name='like_post'),

]