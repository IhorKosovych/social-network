from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
 
 
class Posst(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE,
    )
    body = models.TextField()
    likes = models.ManyToManyField(User, blank=True, related_name='likes')
    post_like = models.IntegerField('like', default = 0)
    post_dislike = models.IntegerField('dislike', default = 0)
 
    def __str__(self):
        return self.title

    @property
    def total_dislikes(self):
        return self.post_dislike.count()

class Like(models.Model):
    LIKE_OR_DISLAKE_CHOICES = (
    ("LIKE", "like"),
    ("DISLIKE", "dislike"),
    (None, "None")
    )
 
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    for_post = models.ForeignKey(Posst, on_delete = models.CASCADE)
    like_or_dislike = models.CharField(max_length=7,
                  choices=LIKE_OR_DISLAKE_CHOICES,
                  default=None)