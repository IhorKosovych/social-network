from django.shortcuts import render

# Create your views here.

from django.views.generic import ListView, DetailView
from django.shortcuts import render, redirect
from .forms import PostCreate
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.views.generic import RedirectView
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

import json

from .models import Posst, Like
 
 
class BlogListView(ListView):
    model = Posst
    template_name = 'home2.html'

def upload(request):
    upload = PostCreate()
    if request.method == 'POST':
        upload = PostCreate(request.POST, request.FILES)
        if upload.is_valid():
            upload.save()
            return redirect('/')
        else:
            return HttpResponse("""your form is wrong, reload on <a href = "{{ url : 'index'}}">reload</a>""")
    else:
        return render(request, 'upload_form.html', {'upload_form':upload})


def post_detail(request, pk):
    post = Posst.objects.get(pk=pk)
    context = {
        'post': post
    }
    return render(request, 'post_detail.html', context)

def like_view(request, pk):
    post = get_object_or_404(Posst, id=request.POST.get('post_id'))
    post.likes.add(request.user)
    return HttpResponseRedirect(reverse('post_detail', args=[str(pk)]))